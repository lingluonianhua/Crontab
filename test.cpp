// Cron.cpp : 定义控制台应用程序的入口点。
//

#include "Crontab.h"
#include <iostream>
#include <map>
#include <string>
#include <time.h>

void TestCron(const char* cronExpr, const char* pszTime, bool nRet)
{
	int nYear, nMon, nDay, nHour, nMin, nSec;
	_snscanf_s(pszTime, strlen(pszTime), "%d-%d-%d %d:%d:%d", &nYear, &nMon, &nDay, &nHour, &nMin, &nSec);
	CCrontab cron(nYear, nMon, nDay, nHour, nMin, nSec);
	if (cron.CompTime(cronExpr) == nRet)
	{
		std::cout << pszTime << " " << cronExpr << " : " << "True" << std::endl;
	}
	else
	{
		std::cout << pszTime << " " << cronExpr << " : " << "False" << std::endl;
	}
	
}

void TestRunTask(std::map<std::string, std::string>& vecTaskList)
{
	while (true)
	{
		struct tm timeInfo;
		time_t currTime = time(NULL);
		localtime_s(&timeInfo, &currTime);
		CCrontab cron;
		for (auto item : vecTaskList)
		{
			if (cron.CompTime(item.second.c_str()))
			{
				std::cout << "start task " << "Time: " << timeInfo.tm_hour << ":" << timeInfo.tm_min << ":" << timeInfo.tm_sec << "---->" << item.first << std::endl;
			}
		}
		_sleep(1000);
	}
}

int main()
{
	TestCron("0/5 15,30,50 12 * * * ", "2019-02-21 12:30:00", true);
	TestCron("0/5 15,30,50 12 * * * ", "2019-02-21 12:30:01", false);
	TestCron("0/5 15,30,50 12 * * * ", "2019-02-21 13:30:00", false);
	TestCron("0/5 15,30,50 12 * * * ", "2019-02-21 12:30:05", true);
	TestCron("0 15-50 11-15 21 * * ", "2019-02-21 11:50:00", true);
	TestCron("0 15-50 11-15 20 * * ", "2019-02-21 11:50:00", false);
	TestCron("0 15 10 * * 6L ", "2019-01-25 10:15:00", true); //每月最后一个星期五10点15分执行
	TestCron("0 15 10 * * 6L ", "2019-02-01 10:15:00", false); 
	TestCron("0 15 10 * * 6L ", "2019-01-24 10:15:00", false); 

	TestCron("0 15 10 LW * * ", "2019-01-31 10:15:00", true); //最后一个工作日
	TestCron("0 15 10 LW * * ", "2019-01-30 10:15:00", false); //最后一个工作日

	TestCron("0 15 10 L * * ", "2019-01-31 10:15:00", true);  //每个月最后一天
	TestCron("0 15 10 30W * * ", "2019-01-30 10:15:00", true);  //每个月30号最近的工作日
	TestCron("0 15 10 26W * * ", "2019-01-25 10:15:00", true);  //每个月30号最近的工作日
	TestCron("0 15 10 26W * * ", "2019-01-26 10:15:00", false);  //每个月30号最近的工作日
	TestCron("0 15 10 26W * * ", "2019-01-28 10:15:00", false);  //每个月30号最近的工作日

	TestCron("0 15 10 * * 6#2 ", "2019-01-11 10:15:00", true);  //每个月第二个星期五
	TestCron("0 15 10 * * 6#2 ", "2019-01-04 10:15:00", false);  //每个月第二个星期五
	TestCron("0 15 10 * * 6#2 ", "2019-01-18 10:15:00", false);  //每个月第二个星期五


	std::map<std::string, std::string> vecTaskList = {
		{ "task1", "0 1/3 "},
		{ "task2", "0 30 " },
		{ "task3", "10,30 1/3 "},
		{ "task4", "0 20-30 " },
		{ "task5", "5/15 10/5 " },
		{ "task6", "0 2/15 " },
		{ "task7", "30 10/15 " },
		{ "task8", "0 30 " },
		{ "task9", "0 45 " },
		{ "task10","0 59 " }
	};

	TestRunTask(vecTaskList);
    return 0;
}

