#include "Crontab.h"
#include <string>
#include <time.h>
#include "platform.h"

CCrontab::CCrontab()
{
	time_t currTime = time(NULL);
	
	localtime_ex(&m_ptminfo, &currTime);
}

CCrontab::CCrontab(int iYear, int iMonth, int iDay,
	int iHour, int iMinute, int iSecond)
{
	struct tm timeInfo;
	timeInfo.tm_year = iYear - 1900;
	timeInfo.tm_mon = iMonth - 1;
	timeInfo.tm_mday = iDay;
	timeInfo.tm_hour = iHour;
	timeInfo.tm_min = iMinute;
	timeInfo.tm_sec = iSecond;
	auto setTm = mktime(&timeInfo);
	localtime_ex(&m_ptminfo, &setTm);
}
CCrontab::~CCrontab()
{
}

std::string CCrontab::ReplaceMonth(const std::string& strVal)
{
	std::string strNewVal = strVal;
	char *pszMonth[] = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV","DEC" };
	for (int i = 0; i < 12; i++)
	{
		const char* pszCurrMon = pszMonth[i];
		int nPos = strNewVal.find(pszCurrMon);
		if (nPos != std::string::npos)
		{
			std::string repStr = std::to_string(i);
			strNewVal.replace(nPos, nPos + 3, repStr.c_str(), repStr.size());
		}
	}
	return strNewVal;
}

std::string CCrontab::ReplaceWeek(const std::string& strVal)
{
	std::string strNewVal = strVal;
	char *pszWeek[] = { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };
	for (int i = 0; i < 7; i++)
	{
		const char* pszCurrWeek = pszWeek[i];
		int nPos = strNewVal.find(pszCurrWeek);
		if (nPos != std::string::npos)
		{
			std::string repStr = std::to_string(i);
			strNewVal.replace(nPos, nPos + 3, repStr.c_str(), repStr.size());
		}
	}
	return strNewVal;
}

bool CCrontab::CompTime(const char * pszCronExpr)
{
	//����ʱ��  ����Χ �����
	std::string strValue;
	std::vector<std::string> strValueList;
	bool bRang = false;
	bool nCycle = false;
	int nStep = 0;
	bool bLast = false, bEffect = false, bTheWeek = false;
	const char *pszPtr = pszCronExpr;
	char ch;
	while ( (ch = *pszPtr++))
	{
		switch (ch)
		{
		case ' ':
			bool nRet;
			if (bRang)
			{
				if (strValueList.size() < 1)
				{
					return false;
				}
				nRet = CompNodeRange(nStep, atoi(strValueList.front().c_str()), atoi(strValue.c_str()));
			}
			else if (nCycle)
			{
				if (strValueList.size() < 1)
				{
					return false;
				}
				nRet = CompNodeCycle(nStep, atoi(strValueList.front().c_str()), atoi(strValue.c_str()));
			}
			else if (bLast && bEffect)
			{
				nRet = CompNodeLastEffect(nStep, strValue);
			}
			else if (bLast)
			{
				nRet = CompNodeLast(nStep, strValue);
			}
			else if (bEffect)
			{
				nRet = CompNodeEffective(nStep, strValue);
			}
			else if (bTheWeek)
			{
				nRet = CompNodeOfWeek(nStep, strValueList.front(), strValue);
			}
			else
			{
				if (strValueList.size() == 0)
				{
					nRet = CompNodeValue(nStep, strValue);
				}
				else
				{
					strValueList.push_back(strValue);
					nRet = CompNodeValueList(nStep, strValueList);
				}
				
			}
			if (!nRet)
			{
				return false;
			}
			bRang = false;
			nCycle = false;
			bLast = false;
			bEffect = false;
			bTheWeek = false;
			nStep++;
			strValueList.clear();
			strValue.clear();
			break;
		case ',':
			strValueList.push_back(strValue);
			strValue.clear();
			break;
		case '-':
			strValueList.push_back(strValue);
			strValue.clear();
			bRang = true;
			break;
		case '/':
			strValueList.push_back(strValue);
			strValue.clear();
			nCycle = true;
			break;
		case 'L':
			bLast = true;
			break;
		case 'W':	
			bEffect = true;
			break;
		case '#':
			strValueList.push_back(strValue);
			strValue.clear();
			bTheWeek = true;
			break;
		default:
			strValue += ch;
			break;
		}
	}

	return true;
}


bool CCrontab::CompNodeRange(int nStep, int nBeg, int nEnd)
{
	int nVal = GetTypeValue(nStep);
	return nVal >= nBeg && nVal <= nEnd;
}

bool CCrontab::CompNodeCycle(int nStep, int nVal, int nCycle)
{
	int nCurrVal = GetTypeValue(nStep);
	int addTime = nCurrVal - nVal;
	if (addTime < 0)
	{
		return false;
	}
	return addTime%nCycle == 0;
}


bool CCrontab::CompNodeValueList(int nStep, std::vector<std::string>& strValueList)
{
	int nCurrVal = GetTypeValue(nStep);
	for (auto item : strValueList)
	{
		if (atoi(item.c_str()) == nCurrVal)
		{
			return true;
		}
	}
	return false;
}

bool CCrontab::CompNodeValue(int nStep,const std::string& strValue)
{
	if (strValue == "*")
	{
		return true;
	}
	int nCurrVal = GetTypeValue(nStep);
	return atoi(strValue.c_str()) == nCurrVal;
}

bool CCrontab::CompNodeLast(int nStep, const std::string& strVal)
{
	int dayOfMon[] = { 31,30,31,30,31,30,31,31,30,31,30,31 };
	int lastVal = atoi(strVal.c_str());
	if (nStep == 3)
	{	
		return dayOfMon[m_ptminfo.tm_mon] - lastVal == m_ptminfo.tm_mday;
	}
	else if (nStep == 5)
	{
		return m_ptminfo.tm_wday + 1 == lastVal && dayOfMon[m_ptminfo.tm_mon] - m_ptminfo.tm_mday < 7;
	}
	return false;
}

bool CCrontab::CompNodeEffective(int nStep, const std::string& strVal)
{
	int effVal = atoi(strVal.c_str());
	if (nStep == 3)
	{
		return (m_ptminfo.tm_mday == effVal && m_ptminfo.tm_wday >= 1 && m_ptminfo.tm_wday <= 5)
			|| (m_ptminfo.tm_mday + 1 == effVal && m_ptminfo.tm_wday == 5)
			|| (m_ptminfo.tm_mday - 1 == effVal && m_ptminfo.tm_wday == 0);
	}
	return false;
}

bool CCrontab::CompNodeLastEffect(int nStep, const std::string& strVal)
{
	int dayOfMon[] = { 31,30,31,30,31,30,31,31,30,31,30,31 };
	if (nStep == 3)
	{
		return (m_ptminfo.tm_mday == dayOfMon[m_ptminfo.tm_mon] && m_ptminfo.tm_wday >= 1 && m_ptminfo.tm_wday <= 5)
			|| (m_ptminfo.tm_mday + 1 == dayOfMon[m_ptminfo.tm_mon] && m_ptminfo.tm_wday == 5)
			|| (m_ptminfo.tm_mday + 2 == dayOfMon[m_ptminfo.tm_mon] && m_ptminfo.tm_wday == 5);
	}
	return false;
}


bool CCrontab::CompNodeOfWeek(int nStep, const std::string& strWeek, const std::string& strIdx)
{
	if (nStep == 5)
	{
		return atoi(strWeek.c_str()) == m_ptminfo.tm_wday + 1
			&& m_ptminfo.tm_mday < 7 * atoi(strIdx.c_str()) 
			&& m_ptminfo.tm_mday > 7 *( atoi(strIdx.c_str()) - 1);
	}
	return false;
}

int CCrontab::GetTypeValue(int nStep)
{
	switch (nStep)
	{
	case 0:
		return m_ptminfo.tm_sec;
	case 1:
		return m_ptminfo.tm_min;
	case 2:
		return m_ptminfo.tm_hour;
	case 3:
		return m_ptminfo.tm_mday;
	case 4:
		return m_ptminfo.tm_mon + 1;
	case 5:
		return m_ptminfo.tm_wday ;
	case 6:
		return m_ptminfo.tm_year + 1900;
	default:
		break;
	}
	return -1;
}