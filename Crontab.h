#ifndef __CRONTAB_H__
#define __CRONTAB_H__
#include <vector>
#include <string>

class CCrontab
{
public:
	CCrontab();
	CCrontab(int iYear, int iMonth, int iDay, int iHour, int iMinute, int iSecond);
	~CCrontab();

	bool CompTime(const char* pszCronExpr);

private:
	std::string ReplaceMonth(const std::string & strVal);

	std::string ReplaceWeek(const std::string & strVal);

	
	bool CompNodeRange(int nStep, int nBeg, int nEnd);
	bool CompNodeCycle(int nStep, int nVal, int nCycle);

	bool CompNodeValueList(int nStep, std::vector<std::string>& strValueList);
	bool CompNodeValue(int nStep, const std::string & strValue);
	bool CompNodeLast(int nStep, const std::string & strVal);
	bool CompNodeEffective(int nStep, const std::string & strVal);
	bool CompNodeLastEffect(int nStep, const std::string & strVal);
	bool CompNodeOfWeek(int nStep, const std::string& strWeek, const std::string& strIdx);

	int GetTypeValue(int nStep);

private:
	struct tm m_ptminfo;
};

#endif //__CRONTAB_H__